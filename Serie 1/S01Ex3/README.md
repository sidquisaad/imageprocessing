## The `FileImage` structure :
Hold information and data of a file image. Members :
 
- `name` : Image name. Maximum of 48 characters.
- `height` : Height of image.
- `width` : width of image
- `depth` : Image bit depth.
- `size` : Image size in bytes.
- `description` : Image description. Maximum of 64 characters.
- `data` : Array of arrays holding image data. To access the **pixel at (x, y)** use **`data[y][x]`** 

## Included Functions :
### allocateFileImage(`width`, `height`) :
Allocates an array of arrays to hold the image in memory. Returns everything wrapped in an 8-bit `FileImage` structure.

### freeFileImage(`FileImage`) :
Frees allocated memory of the given `FileImage`.

### loadFromRAW(`fileName`, `width`, `height`) :
Loads image data from a raw file, and returns it in an 8-bit `FileImage` structure.

### writeFileImageToFile(`FileImage`, `fileName`) :
Writes a binary file `fileName` containing the given `FileImage`.

### writeRAWImageToFile(`FileImage`, `fileName`) :
Writes a binary file `fileName` containing raw `data` from the given `FileImage`.
 
### reduceImageBitDepth(`FileImage`, `bits`) :
Returns a copy of the given `FileImage` with a bit-depth reduced to `bits`.

### resizeImage(`FileImage`, `factor`) :
Returns a `factor` times smaller copy of the given `FileImage`. Uses a 3x3 simple average interpolation.