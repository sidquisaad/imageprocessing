#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const unsigned char GS8_BLACK = 0;               // 8-bit grayscale black.
const unsigned char GS8_WHITE = 255;             // 8-bit grayscale white.

typedef struct FileImage FileImage;
struct FileImage {
	char                    name[48];
	unsigned int            height;
	unsigned int            width;
	unsigned int            depth;
	unsigned int            size;
	char                    description[64];
	unsigned char**         data;
};


FileImage allocateFileImage (unsigned int width, unsigned int height) {
	FileImage result;

	result.height = height;
	result.width = width;
	result.depth = sizeof(unsigned char) * 8;
	result.size = result.width * result.height * sizeof(unsigned char);

	result.data = (unsigned char**) malloc(result.height * sizeof(unsigned char*));

	for (unsigned int y = 0; y < result.height; y++)
		result.data[y]= (unsigned char *) malloc(result.width * sizeof(unsigned char));

	printf("allocated %u x %u\n", width, height);
	return  result;
}

void freeFileImage (FileImage src) {
	for (unsigned int y = 0; y < src.height; y++)
		free(src.data[y]);

	free(src.data);
	printf("freed %u x %u\n", src.width, src.height);
}

void writeFileImageToFile(FileImage src, char fileName[]) {
	FILE* file;

	struct FileImageHeader {
		char                    name[48];
		unsigned int            height;
		unsigned int            width;
		unsigned int            depth;
		unsigned int            size;
		char                    description[64];
	} t;

	t.height = src.height;
	t.width = src.width;
	t.depth = src.depth;
	t.size = src.size;
	memset(t.name, 0, sizeof(t.name));
	memset(t.description, 0, sizeof(t.description));
	strncpy(t.name, src.name, sizeof(src.name) - 1);
	strncpy(t.description, src.description, sizeof(src.description) - 1);

	unsigned long headerSize = sizeof(t);
	unsigned int written;

	file = fopen(fileName, "wb");
	if (file == NULL) {
		printf("Cannot open '%s' for writing\n.", fileName);
		exit(-1);
	}

	if (fwrite(&t, headerSize, 1, file) != 1){
		printf("Write error.\n");
		exit(-1);
	}

	for (unsigned int y = 0; y < src.height; y++) {
		written = fwrite(src.data[y], sizeof(unsigned char), src.width, file);
		if (written != src.width) {
			printf("Write error row %d.\n", y);
			exit(-1);
		}
	}

	printf("Wrote %lu to file '%s', %u image data, %lu header.\n", headerSize + src.size, fileName, src.size, headerSize);
	fclose(file);
}

FileImage createCheckerboard(unsigned int boardSize, unsigned int cellsPerLine) {
	unsigned int cellSize = boardSize / cellsPerLine;
	unsigned char col = GS8_BLACK;
	FileImage img = allocateFileImage(boardSize, boardSize);

	if (cellSize < 1) {
		printf("Invalid board size or cells per line provided.");
		exit(-1);
	}

	for (unsigned int y = 0; y < img.height; y++) {
		if (y % cellSize == 0)
			col = ~col;
		for (unsigned int x = 0; x < img.width; x++) {
			if (x % cellSize == 0)
				col = ~col;

			img.data[y][x] = col;
		}
	}

	return img;
}

int main () {
	FileImage img = createCheckerboard(1024, 8);

	printf("Enter an image name (48 char. max):\n");
	fgets(img.name, sizeof(img.name), stdin);

	printf("Enter an image description (68 char. max):\n");
	fgets(img.description, sizeof(img.description), stdin);

	writeFileImageToFile(img, "output/checkerboard.raw");

	freeFileImage(img);
	return 0;
}


