## The `FileImage` structure :
Hold information and data of a file image. Members :
 
- `name` : Image name. Maximum of 48 characters.
- `height` : Height of image.
- `width` : width of image
- `depth` : Image bit depth.
- `size` : Image size in bytes.
- `description` : Image description. Maximum of 64 characters.
- `data` : Array of arrays holding image data. To access the **pixel at (x, y)** use **`data[y][x]`** 

## Included Functions :
### allocateFileImage(`width`, `height`) :
Allocate an array of arrays to hold the image in memory. Returns everything wrapped in an 8-bit `FileImage` structure.

### freeFileImage(`FileImage`) :
Frees allocated memory of the given `FileImage`.

### writeFileImageToFile(`FileImage`, `fileName`) :
Writes a binary file `fileName` containing the given `FileImage`.

### createCheckerboard(`boardSize`, `cellsPerLine`) :
Creates a checkerboard of size `boardSize` containing `cellsPerLine` x `cellsPerLine` cells.

