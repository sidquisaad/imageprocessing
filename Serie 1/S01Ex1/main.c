#include <stdio.h>
#include <stdlib.h>

const unsigned char GS8_BLACK = 0;               // 8-bit grayscale black.
const unsigned char GS8_WHITE = 255;             // 8-bit grayscale white.

typedef struct MemoryImage MemoryImage;
struct MemoryImage {
	unsigned int            height;
	unsigned int            width;
	unsigned char**         data;
};

MemoryImage allocateMemoryImage (unsigned int width, unsigned int height) {
	MemoryImage result;

	result.height = height;
	result.width = width;

	result.data = (unsigned char**) malloc(result.height * sizeof(unsigned char*));

	for (unsigned int y = 0; y < result.height; y++)
		result.data[y]= (unsigned char *) malloc(result.width * sizeof(unsigned char));

	printf("allocated %d x %d\n", width, height);
	return  result;
}

void freeMemoryImage (MemoryImage src) {
	for (unsigned int y = 0; y < src.height; y++)
		free(src.data[y]);

	free(src.data);
	printf("freed %d x %d\n", src.width, src.height);
}

void writeMemoryImageToFile(MemoryImage src, char fileName[]) {
	FILE* file;

	file = fopen(fileName, "wb");
	if (file == NULL) {
		printf("Cannot open '%s' for writing.\n", fileName);
		exit(-1);
	}

	for (unsigned int y = 0; y < src.height; y++) {
		if (fwrite(src.data[y], sizeof(unsigned char), src.width, file) != src.width) {
			printf("Write error row %d.\n", y);
			exit(-1);
		}
	}
	printf("Wrote %d x %d to file '%s'\n", src.width, src.height, fileName);
	fclose(file);
}

MemoryImage makeVerticalBands (unsigned int width, unsigned int height) {
	MemoryImage img = allocateMemoryImage(width, height);

	unsigned char col = GS8_BLACK;

	for (unsigned int y = 0; y < img.height; y++) {
		for (unsigned int x = 0; x < img.width; x++) {
			if (x % 8 == 0)
				col = ~col;
			img.data[y][x] = col;
		}
	}

	return img;
}

MemoryImage flip90CW (MemoryImage src) {
	MemoryImage result = allocateMemoryImage(src.height, src.width);

	for (unsigned int y = 0; y < result.height; y++) {
		for (unsigned int x = 0; x < result.width; x++) {
			result.data[y][x] = src.data[src.height - (x + 1)][y];
		}
	}

	return  result;
}

MemoryImage xorMemoryImages (MemoryImage a, MemoryImage b) {

	if ((a.width != b.width) || (a.height != b.height)) {
		printf("Images should have the same size");
		exit(-1);
	}

	MemoryImage result = allocateMemoryImage(a.height, b.width);

	for (unsigned int y = 0; y < result.height; y++) {
		for (unsigned int x = 0; x < result.width; x++) {
			result.data[y][x] = a.data[y][x] ^ b.data[y][x];
		}
	}

	return  result;
}

int main () {
	MemoryImage bandsVert = makeVerticalBands(64, 64);
	MemoryImage bandsHoriz = flip90CW(bandsVert);
	MemoryImage chessBoard = xorMemoryImages(bandsVert, bandsHoriz);

	writeMemoryImageToFile(bandsVert,"output/vertical.raw");
	writeMemoryImageToFile(bandsHoriz,"output/horizontal.raw");
	writeMemoryImageToFile(chessBoard,"output/checkerboard.raw");

	freeMemoryImage(chessBoard);
	freeMemoryImage(bandsHoriz);
	freeMemoryImage(bandsVert);
	return 0;
}


