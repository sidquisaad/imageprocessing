## The `MemoryImage` structure :
Hold information and data of a memory image. Members : 

- `height` : Height of image.
- `width` : width of image
- `data` : Array of arrays holding image data. To access the **pixel at (x, y)** use **`data[y][x]`** 

## Included Functions :
### allocateMemoryImage(`width`, `height`) :
Allocate an array of arrays to hold the image in memory. Returns everything wrapped in a `MemoryImage` structure.

### freeMemoryImage(`memoryImage`) :
Frees allocated memory of the given `MemoryImage`.

### writeMemoryImageToFile(`memoryImage`, `fileName`) :
Writes a binary file `fileName` containing raw `data` from the given `MemoryImage`.

### makeVerticalBands(`width`, `height`) :
Returns an `MemoryImage` of size `width` x `height`, containing 8 pixel wide black and white stripes.

### flip90CW(`memoryImage`) : 
Returns a copy of the given `MemoryImage` flipped 90 degrees clock-wise.

### xorMemoryImages(`memoryImage`, `memoryImage`) :
Returns a new `MemoryImage` where each pixel is the result of a binary XOR of the pixels at the same position in the given `MemoryImage` structures. 