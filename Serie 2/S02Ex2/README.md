## Important Notice :
The histogram data is printed out in the console. If a fixed-width font is used, is shown up as a nicely formatted table. 

## The `FileImage` structure :
Hold information and data of a file image. Members :
 
- `name` : Image name. Maximum of 48 characters.
- `height` : Height of image.
- `width` : width of image
- `depth` : Image bit depth.
- `size` : Image size in bytes.
- `description` : Image description. Maximum of 64 characters.
- `data` : Array of arrays holding image data. To access the **pixel at (x, y)** use **`data[y][x]`** 

## Included Functions :
### allocateFileImage(`width`, `height`) :
Allocates an array of arrays to hold the image in memory. Returns everything wrapped in an 8-bit `FileImage` structure.

### freeFileImage(`FileImage`) :
Frees allocated memory of the given `FileImage`.

### loadFromRAW(`fileName`, `width`, `height`) :
Loads image data from a raw file, and returns it in an 8-bit `FileImage` structure.

### writeFileImageToFile(`FileImage`, `fileName`) :
Writes a binary file `fileName` containing the given `FileImage`.

### writeRAWImageToFile(`FileImage`, `fileName`) :
Writes a binary file `fileName` containing raw `data` from the given `FileImage`.
 
### fillHistogramFromFileImage(`FileImage`, `histogram`) :
Fills the array pointed by `histogram` with the histogram of the given `FileImage`.