#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const unsigned char GS8_BLACK = 0;               // 8-bit grayscale black.
const unsigned char GS8_WHITE = 255;             // 8-bit grayscale white.

typedef struct FileImage FileImage;
struct FileImage {
	char                    name[48];
	unsigned int            height;
	unsigned int            width;
	unsigned int            depth;
	unsigned int            size;
	char                    description[64];
	unsigned char**         data;
};

FileImage allocateFileImage (unsigned int width, unsigned int height) {
	FileImage result;

	result.height = height;
	result.width = width;
	result.depth = sizeof(unsigned char) * 8;
	result.size = result.width * result.height * sizeof(unsigned char);

	memset(result.name, 0, sizeof(result.name));
	memset(result.description, 0, sizeof(result.description));

	result.data = (unsigned char**) malloc(result.height * sizeof(unsigned char*));

	for (unsigned int y = 0; y < result.height; y++)
		result.data[y]= (unsigned char *) malloc(result.width * sizeof(unsigned char));

	printf("allocated %u x %u\n", width, height);
	return  result;
}

void freeFileImage (FileImage src) {
	for (unsigned int y = 0; y < src.height; y++)
		free(src.data[y]);

	free(src.data);
	printf("freed %u x %u\n", src.width, src.height);
}

FileImage loadFromRAW(char* fileName, unsigned int width, unsigned int height) {
	FILE* file;
	unsigned int read;
	unsigned int totalRead = 0;
	FileImage img = allocateFileImage(width, height);

	file = fopen(fileName, "rb");
	if (file == NULL) {
		printf("Cannot open '%s' for reading.\n", fileName);
		exit(-1);
	}
	for (unsigned int y = 0; y < img.height; y++) {
		read = fread(img.data[y], sizeof(unsigned char), img.width, file);
		if (read != img.width) {
			printf("Read error on row %d\n", y);
			exit(-1);
		}
		totalRead = totalRead + read;
	}

	strncpy(img.name, fileName, sizeof(img.name) - 1);

	printf("Read %u from file '%s'\n", totalRead, fileName);
	fclose(file);

	return img;
}

void writeFileImageToFile(FileImage src, char fileName[]) {
	FILE* file;

	struct FileImageHeader {
		char                    name[48];
		unsigned int            height;
		unsigned int            width;
		unsigned int            depth;
		unsigned int            size;
		char                    description[64];
	} t;

	t.height = src.height;
	t.width = src.width;
	t.depth = src.depth;
	t.size = src.size;
	memset(t.name, 0, sizeof(t.name));
	memset(t.description, 0, sizeof(t.description));
	strncpy(t.name, src.name, sizeof(src.name) - 1);
	strncpy(t.description, src.description, sizeof(src.description) - 1);

	unsigned long headerSize = sizeof(t);
	unsigned int written;

	file = fopen(fileName, "wb");
	if (file == NULL) {
		printf("Cannot open '%s' for writing\n.", fileName);
		exit(-1);
	}

	if (fwrite(&t, headerSize, 1, file) != 1){
		printf("Write error.\n");
		exit(-1);
	}

	for (unsigned int y = 0; y < src.height; y++) {
		written = fwrite(src.data[y], sizeof(unsigned char), src.width, file);
		if (written != src.width) {
			printf("Write error row %d.\n", y);
			exit(-1);
		}
	}

	printf("Wrote %lu to file '%s', %u image data, %lu header.\n", headerSize + src.size, fileName, src.size, headerSize);
	fclose(file);
}

void writeRAWImageToFile(FileImage src, char* fileName) {
	FILE* file;
	unsigned int written;
	file = fopen(fileName, "wb");
	if (file == NULL) {
		printf("Cannot open '%s' for writing.\n", fileName);
		exit(-1);
	}

	for (unsigned int y = 0; y < src.height; y++) {
		written = fwrite(src.data[y], sizeof(unsigned char), src.width, file);
		if (written != src.width) {
			printf("Write error row %d.\n", y);
			printf("%u vs %u\n", written, src.width);
			exit(-1);
		}
	}
	printf("Wrote %d x %d to file '%s'\n", src.width, src.height, fileName);
	fclose(file);
}

void fillHistogramFromFileImage(FileImage src, unsigned int histogram[static 256]) {
	memset(histogram, 0, 256 * sizeof(unsigned int));

	for (unsigned int y = 0; y < src.height; y++) {
		for (unsigned int x = 0; x < src.width; x++) {
			histogram[src.data[y][x]]++;
		}
	}
}

int main () {
	FileImage input = loadFromRAW("input/colline_385x289.raw", 385, 289);
	unsigned int histogram[256];

	fillHistogramFromFileImage(input, histogram);

	printf("-------------------------\nx\t|\tH(x)\n-------------------------\n");
	for (unsigned int i = 0; i < 256; i++)
		printf("%u\t|\t%u\n", i, histogram[i]);

	freeFileImage(input);
	return 0;
}