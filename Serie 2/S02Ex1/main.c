#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

const unsigned char GS8_BLACK = 0;               // 8-bit grayscale black.
const unsigned char GS8_WHITE = 255;             // 8-bit grayscale white.

typedef struct RGB RGB;
struct RGB {
	unsigned char           r;
	unsigned char           g;
	unsigned char           b;
};

typedef struct FileImageRGB FileImageRGB;
struct FileImageRGB {
	char                    name[48];
	unsigned int            height;
	unsigned int            width;
	unsigned int            depth;
	unsigned int            size;
	char                    description[64];
	RGB**                   data;
};

FileImageRGB allocateFileImageRGB (unsigned int width, unsigned int height) {
	FileImageRGB result;

	result.height = height;
	result.width = width;
	result.depth = sizeof(unsigned char) * 8;
	result.size = result.width * result.height * sizeof(RGB);

	memset(result.name, 0, sizeof(result.name));
	memset(result.description, 0, sizeof(result.description));

	result.data = (RGB**) malloc(result.height * sizeof(RGB*));

	for (unsigned int y = 0; y < result.height; y++)
		result.data[y]= (RGB *) malloc(result.width * sizeof(RGB));

	printf("allocated %u x %u\n", width, height);
	return  result;
}

void freeFileImageRGB (FileImageRGB src) {
	for (unsigned int y = 0; y < src.height; y++)
		free(src.data[y]);

	free(src.data);
	printf("freed %u x %u\n", src.width, src.height);
}

FileImageRGB loadFromRAW(char* fileName, unsigned int width, unsigned int height) {
	FILE* file;
	unsigned int read;
	unsigned int totalRead = 0;
	FileImageRGB img = allocateFileImageRGB(width, height);

	file = fopen(fileName, "rb");
	if (file == NULL) {
		printf("Cannot open '%s' for reading.\n", fileName);
		exit(-1);
	}
	for (unsigned int y = 0; y < img.height; y++) {
		read = fread(img.data[y], sizeof(RGB), img.width, file);
		if (read != img.width) {
			printf("Read error on row %d\n", y);
			exit(-1);
		}
		totalRead = totalRead + read;
	}

	strncpy(img.name, fileName, sizeof(img.name) - 1);

	printf("Read %u (%u bytes) from file '%s'\n", totalRead, totalRead * sizeof(RGB), fileName);
	fclose(file);

	return img;
}

void writeFileImageRGBToFile(FileImageRGB src, char* fileName) {
	FILE* file;

	struct FileImageHeaderRGB {
		char                    name[48];
		unsigned int            height;
		unsigned int            width;
		unsigned int            depth;
		unsigned int            size;
		char                    description[64];
	} t;

	t.height = src.height;
	t.width = src.width;
	t.depth = src.depth;
	t.size = src.size;
	memset(t.name, 0, sizeof(t.name));
	memset(t.description, 0, sizeof(t.description));
	strncpy(t.name, src.name, sizeof(src.name) - 1);
	strncpy(t.description, src.description, sizeof(src.description) - 1);

	unsigned long headerSize = sizeof(t);
	unsigned int written;

	file = fopen(fileName, "wb");
	if (file == NULL) {
		printf("Cannot open '%s' for writing\n.", fileName);
		exit(-1);
	}

	if (fwrite(&t, headerSize, 1, file) != 1){
		printf("Header write error.\n");
		exit(-1);
	}

	for (unsigned int y = 0; y < src.height; y++) {
		written = fwrite(src.data[y], sizeof(RGB), src.width, file);
		if (written != src.width) {
			printf("Write error row %d.\n", y);
			exit(-1);
		}
	}

	printf("Wrote %lu to file '%s', %u image data, %lu header.\n", headerSize + src.size, fileName, src.size, headerSize);
	fclose(file);
}

void writeRAWImageRGBToFile(FileImageRGB src, char* fileName) {
	FILE* file;
	unsigned int written;
	file = fopen(fileName, "wb");
	if (file == NULL) {
		printf("Cannot open '%s' for writing.\n", fileName);
		exit(-1);
	}

	for (unsigned int y = 0; y < src.height; y++) {
		written = fwrite(src.data[y], sizeof(RGB), src.width, file);
		if (written != src.width) {
			printf("Write error row %d.\n", y);
			printf("%u vs %u\n", written, src.width);
			exit(-1);
		}
	}
	printf("Wrote %d x %d to file '%s'\n", src.width, src.height, fileName);
	fclose(file);
}

FileImageRGB reduceImageRGBBitDepth (FileImageRGB src, unsigned char bits) {
	FileImageRGB result = allocateFileImageRGB(src.width, src.height);

	unsigned char maximum8 = (unsigned char) (pow(2, 8) - 1);
	unsigned char maximumBits = (unsigned char) (pow(2, bits) - 1);
	unsigned char multiplier = maximum8 / maximumBits;

	if ((bits > 8) || (bits < 1)) {
		printf("Invalid bit depth %u", bits);
		exit(-1);
	}

	for (unsigned int y = 0; y < src.height; y++) {
		for (unsigned int x = 0; x < src.width; x++) {
			result.data[y][x].r = (unsigned char) (round(((float) src.data[y][x].r / maximum8) * maximumBits) * multiplier);
			result.data[y][x].g = (unsigned char) (round(((float) src.data[y][x].g / maximum8) * maximumBits) * multiplier);
			result.data[y][x].b = (unsigned char) (round(((float) src.data[y][x].b / maximum8) * maximumBits) * multiplier);
		}
	}

	return result;
}

FileImageRGB resizeImageRGB (FileImageRGB src, unsigned int factor) {
	if (factor <= 0) {
		printf("Invalid resizing factor %u.\n", factor);
		exit(-1);
	}

	FileImageRGB img = allocateFileImageRGB((unsigned int) floor(src.width / factor), (unsigned int) floor(src.height / factor));

	if ((img.height < 1) || (img.width < 1)) {
		printf("Resizing factor %u is too high for this image.\n", factor);
		exit(-1);
	}

	unsigned int interpolationRange = factor / 2;
	int rangeLeft = -interpolationRange, rangeRight = interpolationRange + 1;
	unsigned int i, j, io, jo, r, g, b, c;

	for (unsigned int y = 0; y < img.height; y++) {
		for (unsigned int x = 0; x < img.width; x++) {
			i = x * factor;
			j = y * factor;
			r = 0; g = 0; b = 0;
			c = 0;
			for (int offsetY = rangeLeft; offsetY < rangeRight; offsetY++) {
				jo = j + offsetY;

				if ((jo < 0) || (jo > (src.height - 1)))
					continue;

				for (int offsetX = rangeLeft; offsetX < rangeRight; offsetX++) {
					io = i + offsetX;

					if ((io < 0) || (io > (src.width - 1)))
						continue;

					r = r + src.data[jo][io].r;
					g = g + src.data[jo][io].g;
					b = b + src.data[jo][io].b;
					c++;
				}
			}
			img.data[y][x].r = (unsigned char) (r / c);
			img.data[y][x].g = (unsigned char) (g / c);
			img.data[y][x].b = (unsigned char) (b / c);
		}
	}

	return img;
}


int main () {
	FileImageRGB input = loadFromRAW("input/nature_1024x768.raw", 1024, 768);

	char buff[256] = {0};

	for (unsigned char i = 1; i < 9; i++) {
		for (unsigned char j = 1; j < 5; j++) {
			FileImageRGB reduced = reduceImageRGBBitDepth(input, i);
			FileImageRGB resized = resizeImageRGB(reduced, j);
			sprintf(buff, "output/output_%dbit_factor_%u__%ux%u.raw", i, j, resized.width, resized.height);
			writeRAWImageRGBToFile(resized, buff);
			freeFileImageRGB(resized);
			freeFileImageRGB(reduced);
		}
	}

	freeFileImageRGB(input);
	return 0;
}