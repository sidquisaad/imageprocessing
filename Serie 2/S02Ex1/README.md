## The `FileImageRGB` structure :
Hold information and data of a file image. Members :
 
- `name` : Image name. Maximum of 48 characters.
- `height` : Height of image.
- `width` : width of image
- `depth` : Image bit depth.
- `size` : Image size in bytes.
- `description` : Image description. Maximum of 64 characters.
- `data` : Array of arrays holding RGB image data. To access the **pixel at (x, y)** use **`data[y][x]`** 

## Included Functions :
### allocateFileImageRGB(`width`, `height`) :
Allocates an array of arrays to hold the image in memory. Returns everything wrapped in an 24-bit RGB `FileImageRGB` structure.

### freeFileImageRGB(`FileImageRGB`) :
Frees allocated memory of the given `FileImageRGB`.

### loadFromRAW(`fileName`, `width`, `height`) :
Loads image data from a raw file, and returns it in an 24-bit RGB `FileImageRGB` structure.

### writeFileImageRGBToFile(`FileImageRGB`, `fileName`) :
Writes a binary file `fileName` containing the given `FileImageRGB`.

### writeRAWImageRGBToFile(`FileImageRGB`, `fileName`) :
Writes a binary file `fileName` containing raw `data` from the given `FileImageRGB`.
 
### reduceImageRGBBitDepth(`FileImageRGB`, `bits`) :
Returns a copy of the given `FileImageRGB` with a bit-depth reduced to `bits`.

### resizeImageRGB(`FileImageRGB`, `factor`) :
Returns a `factor` times smaller copy of the given `FileImageRGB`. Uses a 3x3 simple average interpolation.