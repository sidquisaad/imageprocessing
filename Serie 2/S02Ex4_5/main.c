#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const unsigned char GS8_BLACK = 0;               // 8-bit grayscale black.
const unsigned char GS8_WHITE = 255;             // 8-bit grayscale white.

typedef struct FileImage FileImage;
struct FileImage {
	char                    name[48];
	unsigned int            height;
	unsigned int            width;
	unsigned int            depth;
	unsigned int            size;
	char                    description[64];
	unsigned char**         data;
};

FileImage allocateFileImage (unsigned int width, unsigned int height) {
	FileImage result;

	result.height = height;
	result.width = width;
	result.depth = sizeof(unsigned char) * 8;
	result.size = result.width * result.height * sizeof(unsigned char);

	memset(result.name, 0, sizeof(result.name));
	memset(result.description, 0, sizeof(result.description));

	result.data = (unsigned char**) malloc(result.height * sizeof(unsigned char*));

	for (unsigned int y = 0; y < result.height; y++)
		result.data[y]= (unsigned char *) malloc(result.width * sizeof(unsigned char));

	printf("allocated %u x %u\n", width, height);
	return  result;
}

void freeFileImage (FileImage src) {
	for (unsigned int y = 0; y < src.height; y++)
		free(src.data[y]);

	free(src.data);
	printf("freed %u x %u\n", src.width, src.height);
}

FileImage loadFromRAW(char* fileName, unsigned int width, unsigned int height) {
	FILE* file;
	unsigned int read;
	unsigned int totalRead = 0;
	FileImage img = allocateFileImage(width, height);

	file = fopen(fileName, "rb");
	if (file == NULL) {
		printf("Cannot open '%s' for reading.\n", fileName);
		exit(-1);
	}
	for (unsigned int y = 0; y < img.height; y++) {
		read = fread(img.data[y], sizeof(unsigned char), img.width, file);
		if (read != img.width) {
			printf("Read error on row %d\n", y);
			exit(-1);
		}
		totalRead = totalRead + read;
	}

	strncpy(img.name, fileName, sizeof(img.name) - 1);

	printf("Read %u from file '%s'\n", totalRead, fileName);
	fclose(file);

	return img;
}

void writeFileImageToFile(FileImage src, char fileName[]) {
	FILE* file;

	struct FileImageHeader {
		char                    name[48];
		unsigned int            height;
		unsigned int            width;
		unsigned int            depth;
		unsigned int            size;
		char                    description[64];
	} t;

	t.height = src.height;
	t.width = src.width;
	t.depth = src.depth;
	t.size = src.size;
	memset(t.name, 0, sizeof(t.name));
	memset(t.description, 0, sizeof(t.description));
	strncpy(t.name, src.name, sizeof(src.name) - 1);
	strncpy(t.description, src.description, sizeof(src.description) - 1);

	unsigned long headerSize = sizeof(t);
	unsigned int written;

	file = fopen(fileName, "wb");
	if (file == NULL) {
		printf("Cannot open '%s' for writing\n.", fileName);
		exit(-1);
	}

	if (fwrite(&t, headerSize, 1, file) != 1){
		printf("Write error.\n");
		exit(-1);
	}

	for (unsigned int y = 0; y < src.height; y++) {
		written = fwrite(src.data[y], sizeof(unsigned char), src.width, file);
		if (written != src.width) {
			printf("Write error row %d.\n", y);
			exit(-1);
		}
	}

	printf("Wrote %lu to file '%s', %u image data, %lu header.\n", headerSize + src.size, fileName, src.size, headerSize);
	fclose(file);
}

void writeRAWImageToFile(FileImage src, char* fileName) {
	FILE* file;
	unsigned int written;
	file = fopen(fileName, "wb");
	if (file == NULL) {
		printf("Cannot open '%s' for writing.\n", fileName);
		exit(-1);
	}

	for (unsigned int y = 0; y < src.height; y++) {
		written = fwrite(src.data[y], sizeof(unsigned char), src.width, file);
		if (written != src.width) {
			printf("Write error row %d.\n", y);
			printf("%u vs %u\n", written, src.width);
			exit(-1);
		}
	}
	printf("Wrote %d x %d to file '%s'\n", src.width, src.height, fileName);
	fclose(file);
}

void fillHistogramFromFileImage(FileImage src, double histogram[static 256]) {
	memset(histogram, 0, 256 * sizeof(double));

	for (unsigned int y = 0; y < src.height; y++) {
		for (unsigned int x = 0; x < src.width; x++) {
			histogram[src.data[y][x]]++;
		}
	}
}

FileImage drawHistogram(double histogram[static 256]) {
	const unsigned int WIDTH  = 256;
	const unsigned int HEIGHT = 256;
	unsigned int width_max = WIDTH - 1;
	unsigned int height_max = HEIGHT - 1;

	FileImage img = allocateFileImage(WIDTH, HEIGHT);
	double mul = 0.0;
	unsigned int top;

	// Find the actual maximum in the histogram
	for (unsigned int i = 0; i < WIDTH; i++)
		mul = (histogram[i] > mul ? histogram[i] : mul);

	// Calculate the multiplier
	mul = width_max / mul;

	// Clear image to BLACK
	for (unsigned int y = 0; y < HEIGHT; y++) {
		for (unsigned int x = 0; x < WIDTH; x++) {
			img.data[y][x] = GS8_BLACK;
		}
	}

	// Paint the white regions, effectively leaving the remaining region black to reprsent the data.
	for (unsigned int x = 0; x < WIDTH; x++) {
		top = (unsigned int) (height_max - (histogram[x] * mul));
		for (unsigned int y = 0; y <= top; y++) {
			img.data[y][x] = GS8_WHITE;
		}
	}

	return img;
}

void normalizeHistogram(double histogram[static 256], unsigned int width, unsigned int height) {
	double divider = width * height;
	for (unsigned int i = 0; i < 256; i++) {
		histogram[i] = histogram[i] / divider;
	}
}

void calculateCumulativeHistogram(double normalized[static 256], double cumulative[static 256]) {
	// Clear cumulative histogram
	memset(cumulative, 0, 256 * sizeof(double));

	for (unsigned int i = 0; i < 256; i++)
		cumulative[i] = normalized[i] + (i == 0 ? 0 : cumulative[i - 1]);
}

FileImage applyHistogramToImage(double histogram[static 256], FileImage src) {
	FileImage img = allocateFileImage(src.width, src.height);

	for (unsigned int y = 0; y < img.height; y++) {
		for (unsigned int x = 0; x < img.width; x++) {
			img.data[y][x] = (unsigned char) (histogram[src.data[y][x]] * 255);
		}
	}

	return img;
}

int main () {
	FileImage input = loadFromRAW("input/colline_385x289.raw", 385, 289);
	FileImage histogramNormalized, histogramEqualized, cumulativeHistogram, cumulativeEqualized;
	FileImage output;
	double histogram[256], cumulative[256];

	fillHistogramFromFileImage(input, histogram);

	// Normalize to [0, 1]
	normalizeHistogram(histogram, input.width, input.height);
	histogramNormalized = drawHistogram(histogram);
	writeRAWImageToFile(histogramNormalized, "output/01_histogram_normalized_256x256.raw");
	freeFileImage(histogramNormalized);

	// Calculate cumulative histogram
	calculateCumulativeHistogram(histogram, cumulative);
	cumulativeHistogram = drawHistogram(cumulative);
	writeRAWImageToFile(cumulativeHistogram, "output/02_cumulative_normalized_256x256.raw");
	freeFileImage(cumulativeHistogram);

	// Project
	output = applyHistogramToImage(cumulative, input);
	strncpy(output.name, "Equalized Image", sizeof(output.name) - 1);
	strncpy(output.description, "Equalized Image of colline_385x289.raw", sizeof(output.description) - 1);
	writeFileImageToFile(output, "output/03_image_equalized_128header_colline_385x289.raw");

	// extract Equalized Histogram
	fillHistogramFromFileImage(output, histogram);
	histogramEqualized = drawHistogram(histogram);
	strncpy(histogramEqualized.name, "Histograme égalisé.", sizeof(histogramEqualized.name) - 1);
	strncpy(histogramEqualized.description, "Histograme égalisé de colline_385x289.raw.", sizeof(histogramEqualized.description) - 1);
	writeFileImageToFile(histogramEqualized, "output/04_histogram_equalized_128header_256x256.raw");
	freeFileImage(histogramEqualized);

	calculateCumulativeHistogram(histogram, cumulative);
	cumulativeHistogram = drawHistogram(cumulative);
	writeRAWImageToFile(cumulativeHistogram, "output/05_cumulative_equalized_256x256.raw");
	freeFileImage(cumulativeHistogram);

	freeFileImage(output);
	freeFileImage(input);
	return 0;
}